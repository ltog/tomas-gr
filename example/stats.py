#!/usr/bin/python3

"""
URL: https://github.com/ltog/tomas-gr

stats.py by Lukas Toggenburger

To the extent possible under law, the person who associated CC0 with
stats.py has waived all copyright and related or neighboring rights
to stats.py.

You should have received a copy of the CC0 legalcode along with this
work.  If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
"""

import argparse
import lxml.etree as etree
#import xml.etree.ElementTree as etree

# general pattern from https://stackoverflow.com/a/12161185
def main(args):
    # get an iterable
    context = etree.iterparse(args.infile, events=("start", "end"))

    # turn iterable into an interator
    context = iter(context)

    # get the root element
    event, root = next(context)

    num_sp = 0
    num_hotel = 0
    num_ferienwohnung = 0

    for event, el in context:
        #print("Current tag = " + el.tag)
        #print(event + " " + str(el))
        #print("--------")
        if (event == "end" and el.tag == "{http://www.tbox.ch/dms/xmlstandardexport}ServiceProvider"):
            num_sp += 1

            #tot = el.find(".//TouristicObjectType")
            #if tot != None:
            #    print("TouristicObjectType = " + tot.get("ObjectID"))

            if is_hotel(el):
                num_hotel += 1
            elif is_ferienwohnung(el):
                num_ferienwohnung += 1

            #for child in el:
                #print("child.tag=" + child.tag)

        # don't hog memory
        root.clear()

    print("Got " + str(num_sp) + " serviceproviders out of these " + str(num_hotel) + " are hotels and " + str(num_ferienwohnung) + " are apartments.")

def is_hotel(sp):
    return sp.find(".//TouristicObjectType[@ObjectID='WBX00020010000100214']") != None

def is_ferienwohnung(sp):
    return sp.find(".//TouristicObjectType[@ObjectID='WBX00020010000100218']") != None

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Read TOMAS xml export file and report statistics about it.')
    parser.add_argument("--infile", type=str, help="The file to be read", required=True)

    args = parser.parse_args()

    main(args)


