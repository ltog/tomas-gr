#!/usr/bin/python3

"""
URL: https://github.com/ltog/tomas-gr

create_release.py by Lukas Toggenburger

To the extent possible under law, the person who associated CC0 with
create_release.py has waived all copyright and related or neighboring rights
to create_release.py.

You should have received a copy of the CC0 legalcode along with this
work.  If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.

"""
import argparse
import lxml.etree as etree
import os
import pathlib
import requests
import shutil

def main(args):
    
    # these tags will be removed from all datasets
    doomed_tags = []
    doomed_tags.append("Birthday")

    # delete entire groups of address types from all datasets (all types except "MainAddress")
    doomed_tags.append("ClearingCenter")
    doomed_tags.append("AdditionalAddresses")
    doomed_tags.append("AvisReceiver")
    doomed_tags.append("KeyHolder")
    doomed_tags.append("Owner")

    # these tags will be removed from the CC0 dataset
    nc_only_tags = []
    nc_only_tags.append("Services")

    cc0_input_dir  = "input/cc0/"
    nc_input_dir  = "input/nc/"

    output_dir = "output/"

    tmpfiles = {}

    # TODO: use temporary files according to https://docs.python.org/2/library/tempfile.html ?
    tmpfiles['xml']      = "tmp-tomas-gr.xml"
    tmpfiles['cc0_xml']  = cc0_input_dir + "tomas-gr-cc0.xml"
    tmpfiles['nc_xml']   = nc_input_dir  + "tomas-gr-nc.xml"
    tmpfiles['cc0_date'] = cc0_input_dir + "DATE"
    tmpfiles['nc_date']  = nc_input_dir  + "DATE"

    cc0_zip_file = output_dir + "tomas-gr-cc0"
    nc_zip_file  = output_dir + "tomas-gr-nc"

    # no easily configurable parts below this line
    # ------------------------------------------------------------------------

    download_file(get_url(), tmpfiles["xml"])

    # don't strip CDATA, see https://stackoverflow.com/a/25813863
    parser = etree.XMLParser(strip_cdata=False)
    
    # load existing file into memory
    tree = etree.parse(tmpfiles["xml"], parser=parser)

    # fix URLs with missing http:// prefix
    add_http_prefixes(tree)

    # remove privacy sensitive tags where necessary
    for sp in tree.findall("/{http://www.tbox.ch/dms/xmlstandardexport}ServiceProvider"):
        clear_tags(doomed_tags, sp)

    # start with a clean state
    cleanup(tmpfiles)

    # write NC xml file
    tree.write(tmpfiles["nc_xml"], pretty_print=True, xml_declaration=True, encoding="utf-8")

    # remove NC only tags, since they don't belong into the CC0 dataset
    clear_tags(nc_only_tags, tree)
    
    # write CC0 xml file
    tree.write(tmpfiles["cc0_xml"], pretty_print=True, xml_declaration=True, encoding="utf-8")

    # read export date from xml
    exportdate = tree.getroot().get("ExportDate")

    # write export date to files
    write_string_to_file(exportdate, tmpfiles["cc0_date"])
    write_string_to_file(exportdate, tmpfiles["nc_date"])

    # zip files together
    shutil.make_archive(cc0_zip_file, 'zip', cc0_input_dir) 
    shutil.make_archive(nc_zip_file,  'zip', nc_input_dir) 

    # clean things up
    cleanup(tmpfiles)


def clear_tags(tags, base):
    for tag in tags:
        for doomed_node in base.xpath(".//" + tag):
            remove_node(doomed_node)


def remove_node(node):
    node.getparent().remove(node)


# some URLs in <Internet> are lacking a http:// prefix
# add it if that is the case
def add_http_prefixes(tree):
    for internet in tree.findall(".//Internet"):
        if internet.text != None and \
           '@' not in internet.text and \
           ' ' not in internet.text and \
           internet.text != '' and \
           '.' in internet.text and \
           not internet.text.startswith("http"):

            internet.text = "http://" + internet.text


def download_file(url, dst):
    with open(dst, "wb") as myfile: # https://stackoverflow.com/a/34964610
        response = requests.get(url)
        myfile.write(response.content)


# read url from local file
def get_url():
    url = open("input/url", "r").readline().strip()
    return url

def write_string_to_file(thestring, filename):
    with open(filename, "w", encoding="utf-8") as thefile:
        thefile.write(thestring)

def cleanup(files):
    for k, v in files.items():
        try:
            os.remove(v)
        except OSError: # if file not exists
            pass

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Download TOMAS xml file and prepare it for public release as CC0 and CC BY-NC 4.0 datasets. Input: URL from where to load original file (file `input/url`) Output: 2 zipped xml files (`output/tomas-gr-cc0.zip`, `output/tomas-gr-nc.zip`)')

    args = parser.parse_args()

    main(args)


