#!/usr/bin/python3

import argparse
import lxml.etree as etree
import geojson # see: https://github.com/frewsxcv/python-geojson
import re # regular expressions
import urllib.parse

def main(args):

    # don't strip CDATA, see https://stackoverflow.com/a/25813863
    parser = etree.XMLParser(strip_cdata=False)
    
    # load existing file into memory
    tree = etree.parse(args.infile, parser=parser)

    features = [];

    for sp in tree.findall("/{http://www.tbox.ch/dms/xmlstandardexport}ServiceProvider"):
        lat = get_lat(sp)
        lon = get_lon(sp)

        if is_plausible(lat, lon):
            #print("lat=" + str(lat) + " lon=" + str(lon))
            point = geojson.Point((lon, lat))

            triplets = []
            if is_apartment(sp):
                # don't propose tagging apartments
                tag2list(triplets, sp, ".//MainAddress/CompanyName1", "CompanyName1", "")
                tag2list(triplets, sp, ".//TouristicObjectType/Name/Value[@LanguageCode='de']", "TouristicObjectType", "")
            else:
                tag2list(triplets, sp, ".//MainAddress/CompanyName1", "CompanyName1", get_ahref("name","https://wiki.openstreetmap.org/wiki/DE:Key:name"))
                tag2list(triplets, sp, ".//TouristicObjectType/Name/Value[@LanguageCode='de']", "TouristicObjectType", get_ahref("tourism", "https://wiki.openstreetmap.org/wiki/DE:Key:tourism"))
                tag2list(triplets, sp, ".//Classification/Value[@LanguageCode='de']", "Classification", get_ahref("stars", "https://wiki.openstreetmap.org/wiki/Key:stars"))
                if get_guest_house_type(sp) != "":
                    add_keys_value_to_list("", "guest_house", get_guest_house_type(sp), triplets)
                if is_restaurant(sp):
                    add_keys_value_to_list("", "amenity", "restaurnt", triplets)

            tag2list(triplets, sp, ".//MainAddress/Street",           "Street", get_ahref("addr:street + addr:housenumber /<br/>addr:place /<br/>addr:housename</a>", "https://wiki.openstreetmap.org/wiki/DE:Key:addr"))
            tag2list(triplets, sp, ".//MainAddress/StreetAdditional", "StreetAdditional", get_ahref("addr:place", "https://wiki.openstreetmap.org/wiki/DE:Key:addr") + " ?")
            tag2list(triplets, sp, ".//MainAddress/ZipCode",          "ZipCode", get_ahref("addr:postcode", "https://wiki.openstreetmap.org/wiki/DE:Key:addr"))
            tag2list(triplets, sp, ".//MainAddress/City/DescribingName/Value[@LanguageCode='de']", "City:de", get_ahref("addr:city", "https://wiki.openstreetmap.org/wiki/DE:Key:addr"))
            tag2list(triplets, sp, ".//MainAddress/CountryCode",      "CountryCode", get_ahref("addr:country", "https://wiki.openstreetmap.org/wiki/DE:Key:addr"))

            if is_apartment(sp):
                # don't propose tagging apartments
                tag2list(triplets, sp, ".//MainAddress/Internet", "Internet", "")
            else:
                tag2list(triplets, sp, ".//MainAddress/Internet", "Internet", get_ahref("contact:website", "https://wiki.openstreetmap.org/wiki/DE:Key:contact"))
                tag2list(triplets, sp, ".//MainAddress/Email",    "Email",    get_ahref("contact:email",   "https://wiki.openstreetmap.org/wiki/DE:Key:contact"))
                tag2list(triplets, sp, ".//MainAddress/Phone",    "Phone",    get_ahref("contact:phone",   "https://wiki.openstreetmap.org/wiki/DE:Key:contact"))
                tag2list(triplets, sp, ".//MainAddress/Fax",      "Fax",      get_ahref("contact:fax",     "https://wiki.openstreetmap.org/wiki/DE:Key:contact"))

            images2list(triplets, sp)
            lastmodification2list(triplets, sp)
            rootattr2list(triplets, sp, "ObjectID", "ServiceProvider->ObjectID", "(Eindeutige Identifikationsnummer des Objekts)")
            exportdate2list(triplets, tree)

            properties = {}
            properties['triplets'] = triplets
            properties['is_apartment'] = is_apartment(sp)
            properties['remotecontrol_url'] = get_remotecontrol_url(sp, tree)

            print(get_remotecontrol_url(sp, tree)) # TODO: remove
            #print(find_xpath(".//Classification/Value[@LanguageCode='de']", sp) + "=" + extract_stars(find_xpath(".//Classification/Value[@LanguageCode='de']", sp)))
            #print(find_xpath(".//TouristicObjectType/Name/Value[@LanguageCode='de']", sp))
            #print(prettyprint_phone(find_xpath(".//MainAddress/Phone", sp)) + " num_digits=" + str(num_digits(prettyprint_phone(find_xpath(".//MainAddress/Phone", sp)))))
            #print(prettyprint_phone(find_xpath(".//MainAddress/Fax", sp)) + " num_digits=" + str(num_digits(prettyprint_phone(find_xpath(".//MainAddress/Fax", sp)))))
            #print("classification=" + find_xpath(".//Classification/Value[@LanguageCode='de']", sp) + "bearbeitet=" + extract_stars(find_xpath(".//Classification/Value[@LanguageCode='de']", sp)))

            feature = geojson.Feature(geometry=point, properties=properties)
            features.append(feature)

    feature_collection = geojson.FeatureCollection(features)

    with open(args.outfile, 'w') as f:
        geojson.dump(feature_collection, f)

def extract_stars(classification):
    if classification=="Nicht klassifiziert":
        return ""

    return re.sub(r"([1-5]) Sterne?(?: (S)uperior)?", r"\1\2", classification)

def extract_tourism(tot):
    if tot=="Ferienwohnung":
        return "apartment"
    elif tot=="Hotel":
        return "hotel"
    elif tot=="Camping":
        return "camp_site"
    elif tot=="Ferienhaus":
        return "chalet"
    elif tot=="Gruppenunterkunft/Hütte":
        return "hostel ; alpine_hut" 
    elif tot=="Gästezimmer / B&B":
        return "guest_house"
    elif tot=="Agrotourismus":
        return "guest_house"
    elif tot=="Maiensäss":
        return "chalet"
    else:
        return tot

def get_guest_house_type(sp):
    if find_xpath(".//TouristicObjectType/Name/Value[@LanguageCode='de']", sp) == "Gästezimmer / B&B":
        return "bed_and_breakfast"
    elif find_xpath(".//TouristicObjectType/Name/Value[@LanguageCode='de']", sp) == "Agrotourismus":
        return "agritourism"
    else:
        return ""

def get_lat(sp):
    return float(sp.find('Latitude').text)

def get_lon(sp):
    return float(sp.find('Longitude').text)

def get_remotecontrol_url(sp, tree):
    delta = 0.0001
    bottom = get_lat(sp) - delta
    top    = get_lat(sp) + delta
    left   = get_lon(sp) - 1.5*delta
    right  = get_lon(sp) + 1.5*delta
    url = "http://localhost:8111/load_and_zoom?select=currentselection&bottom=" + str(bottom) + "&top=" + str(top) + "&left=" + str(left) + "&right=" + str(right) + "&addtags="

    tags = [] # contains strings in the form of 'somekey=somevalue'

    if not is_apartment(sp):
        tags.append(create_tag("name", find_xpath(".//MainAddress/CompanyName1", sp)))
        tags.append(create_tag("tourism", extract_tourism(find_xpath(".//TouristicObjectType/Name/Value[@LanguageCode='de']", sp))))
        tags.append(create_tag("stars",   extract_stars(find_xpath(".//Classification/Value[@LanguageCode='de']", sp))))
        tags.append(create_tag("guest_house", get_guest_house_type(sp)))
        if is_restaurant(sp):
            tags.append(create_tag("amenity", "restaurant"))


    (addrstreet,housenumber) = split_street_housenumber(sp)
    tags.append(create_tag("addr:street", addrstreet))
    tags.append(create_tag("addr:housenumber", housenumber))

    # TODO: StreetAdditional ?
    tags.append(create_tag("addr:postcode", find_xpath(".//MainAddress/ZipCode", sp)))
    tags.append(create_tag("addr:city", find_xpath(".//MainAddress/City/DescribingName/Value[@LanguageCode='de']", sp)))
    tags.append(create_tag("addr:country", find_xpath(".//MainAddress/CountryCode", sp)))

    if not is_apartment(sp):
        url_candidate = find_xpath(".//MainAddress/Internet", sp)
        if is_valid_url(url_candidate):
            tags.append(create_tag("contact:website", url_candidate))
        #else:
            #print("invalid url detected=" + url_candidate + "oid=" + sp.get("ObjectID"))

        tags.append(create_tag("contact:email",   find_xpath(".//MainAddress/Email",        sp)))
        tags.append(create_tag("contact:phone",   prettyprint_phone(find_xpath(".//MainAddress/Phone", sp))))
        tags.append(create_tag("contact:fax",     prettyprint_phone(find_xpath(".//MainAddress/Fax",   sp))))

        #print(prettyprint_phone(find_xpath(".//MainAddress/Phone", sp)) + " num_digits=" + str(num_digits(prettyprint_phone(find_xpath(".//MainAddress/Phone", sp)))))
        #print(prettyprint_phone(find_xpath(".//MainAddress/Fax", sp)) + " num_digits=" + str(num_digits(prettyprint_phone(find_xpath(".//MainAddress/Fax", sp)))))

    url += urlify_taglist(tags)

    url += "&changeset_source=" + urllib.parse.quote(create_changeset_source(tree))

    return url

def prettyprint_phone(phone):
    #originalphone = phone

    # 0041 -> +41
    phone = re.sub(r"^\(?004(1|9)\)?", r"+4\1", phone)

    # +41 (0) -> +41
    # +41 0   -> +41
    phone = re.sub(r"^\+ *4(1|9) *\(? *0 *\)? *", r"+4\1 ", phone)

    # 081 -> +41 81
    if num_digits(phone)==10:
        phone = re.sub("^081", "+41 81", phone)
        phone = re.sub("^044", "+41 44", phone)

    # remove brackets, etc. that are still left
    if re.match(r"^\+41", phone):
        phone = re.sub("[^+0-9 ]", "", phone)

    # remove swiss phonenumbers with wrong number of digits
    if re.match(r"^\+41", phone) and not num_digits(phone)==11:
        #print("removed " + phone + " original=" + originalphone)
        return ""

    # reformat phonenumbers
    if re.match(r"^\+41", phone) and num_digits(phone)==11:
        phone = re.sub("[^+0-9]", "", phone)
        phone = re.sub(r"(\+\d\d)(\d\d)(\d\d\d)(\d\d)(\d\d)", r"\1 \2 \3 \4 \5", phone)

    return phone

def num_digits(thestring):
    return sum(c.isdigit() for c in thestring) # https://stackoverflow.com/a/24878232

def split_street_housenumber(sp):
    street_and_number = find_xpath(".//MainAddress/Street", sp)
    if street_and_number != "":
        return re.findall("^\s*(.*?)(?:\s*([0-9]\S*))?\s*$", street_and_number)[0]
    else:
        return ("","")

def create_tag(key, value):
    if value == "":
        return ""
    else:
        return key+"="+value

def find_xpath(xpath, sp):
    node=sp.find(xpath)
    if node != None:
        value = node.text
        if value != None and len(value)>0:
            return sanitize_string(value)
    return ""

def sanitize_string(thestring):
    # remove whitespace at beginning and end
    thestring = re.findall("^\s*(.*?)\s*$", thestring)[0]

    # remove duplicates of whitespace occurrences by their single occurrence
    thestring = re.sub(r'(\s)\1+', r'\1', thestring) # https://stackoverflow.com/a/4574516
    return thestring

def urlify_taglist(tags):
    tags = remove_empty_strings_from_list(tags)
    partial_url = "|".join(tags)
    return urllib.parse.quote(partial_url)
    
def remove_empty_strings_from_list(mylist):
    return filter(None, mylist) # https://stackoverflow.com/a/3845453
    
def exportdate2list(the_list, tree):
    entry = {}
    entry['tomaskey'] = "ExportDate"
    entry['osmkey']   = "Quelle (einzufügen beim Hochladen der Änderung)"
    entry['value']    = create_changeset_source(tree)
    add_entry_to_list(the_list, entry)

def create_changeset_source(tree):
    exportdate = tree.getroot().get("ExportDate")[:-6] # remove timezone information
    return "TOMAS-GR (" + exportdate + ")"

def is_plausible(lat, lon):
    if lat > 45.75 and lat < 47.15 and lon > 8.12 and lon < 10.65:
        return True
    else:
        return False

def is_apartment(sp):
    oid = sp.find("./TouristicObjectType").get("ObjectID")
    if oid=="WBX00020010000100218":
        return True
    else:
        return False

def is_restaurant(sp):
    name = find_xpath(".//MainAddress/CompanyName1", sp)
    if "Restaurant" in name:
        return True
    else:
        return False

def tag2list(the_list, sp, xpath, tomaskey, osmkey):
    value = find_xpath(xpath, sp)

    if value != "":
        add_keys_value_to_list(tomaskey, osmkey, make_ahref_if_url(value), the_list)


def add_keys_value_to_list(tomaskey, osmkey, value, the_list):
    entry = {}
    entry['tomaskey'] = tomaskey
    entry['osmkey']   = osmkey
    entry['value']    = value

    add_entry_to_list(the_list, entry)


def rootattr2list(the_list, sp, attribute, tomaskey, osmkey):
    if sp != None:
        text = sp.get(attribute)
        if text != None: 
            value=str(text)

            entry = {}
            entry['tomaskey'] = tomaskey
            entry['osmkey']   = osmkey
            entry['value']    = make_ahref_if_url(value)

            add_entry_to_list(the_list, entry)

def lastmodification2list(the_list, sp):
    value=sp.get("LastModification")[:-6] # remove timezone information
    if value != None:
        entry = {}
        entry['tomaskey'] = "LastModification"
        entry['osmkey']   = ""
        entry['value']    = make_ahref_if_url(value)

        add_entry_to_list(the_list, entry)
        

def images2list(the_list, sp):
    urls = sp.xpath("./Images/Image/url")
    value = ""
    if urls != None:
        count = 0
        for url in urls:
            count += 1
            urltext = url.text
            if urltext != None:
                value += get_ahref("Image " + str(count), str(urltext)) + " "

        entry = {}
        entry['tomaskey'] = "Images->Image->url"
        entry['osmkey']   = ""
        entry['value']    = value

        add_entry_to_list(the_list, entry)


def add_entry_to_list(the_list, entry):
    the_list.append(entry)

    #print(entry)
    

def make_ahref_if_url(the_string):
    if is_valid_url(the_string):
        return get_ahref(the_string, the_string)
    else:
        return the_string


def get_ahref(text, url):
    return "<a target='_blank' rel='noopener noreferrer' href=" + url + ">" + text + "</a>"


def is_valid_url(url):
    if re.match("^https?://.{2,}", url):
        return True
    else:
        return False


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Read TOMAS xml export file and convert it to GeoJSON.')
    parser.add_argument("--infile",   type=str, help="The file to be read",    default="tomas-gr-cc0.xml")
    parser.add_argument("--outfile",  type=str, help="The file to be written", default="tomas-gr.json")

    args = parser.parse_args()

    main(args)


